import javax.swing.*;

/**
 * @author Enrique Posada y Miguel Navarro on 10/04/2017
 */
public class Main {
/**
 * Clase Main: Se encarga de iniciar el juego, coenzando con abrir un menu hasta que se presiona la tecla espacio y despues comienza el juego
 * @param Args
 */
    public static void main(String[] Args)//metodo main que introduce la ventana Menu y la ventana del juego
    {
        Menu ventanaMenu;
        MiVentana ventana;
        MiCanvas aux;
        HighScores scores = new HighScores();
        int score = 0;

        do {
            ventanaMenu = new Menu();
            ventanaMenu.setSize(600, 600);
            ventanaMenu.setVisible(true);
            ventanaMenu.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

            do {
                System.out.println("menu");
                if (ventanaMenu.signal()) {
                      ventanaMenu.setVisible(false);
                    ventana = new MiVentana();
                    ventana.setSize(620, 700);
                    ventana.setVisible(true);
                    ventana.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
                    do {
                        System.out.println("juego");
                        aux = ventana.getMiCanvas();
                        score = aux.getScore();
                    }while(aux.getLifes()>0);
                    if(aux.getLifes()<=0)
                    {
                        ventana.setVisible(false);
                    }
                }
            } while (!ventanaMenu.signal());

            MenuHighScores newHighScore;
            newHighScore = new MenuHighScores(score);
            newHighScore.setVisible(true);
            newHighScore.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            newHighScore.setSize(350, 350);
            do {
                System.out.println("high scores");
            }while(!newHighScore.getSignal());
            newHighScore.setVisible(false);

        }while(true);
    }
}

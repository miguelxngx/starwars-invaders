import java.awt.*;
import java.awt.image.BufferedImage;
import java.awt.image.ImageObserver;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

/**
 * @author Enrique Posada y Miguel Navarro on 10/04/2017
 * Clase Jugador: Se encarga de trazar y establecer los valores del jugador a trazar en el canvas
 */

public class Jugador {//Introduce todos los valor como posicion, tamanio y paint que requiere el jugador

    private int x, y, score, tamaniox, tamanioy;
    private BufferedImage image;
    private ImageObserver observer;
    private MiImagen imagen;

    /**
     * Constructor Default Jugador: Este constructor se encarga de asignar la imagen que será utilizada 
     para el jugador, así como pone los valores iniciales con los que aparecerá al abrir el juego.
     */
    public Jugador()//Constructor Default
    {
        this.x = 250;
        this.y = 500;
        this.score = 0;
        this.tamaniox = 100;
        this.tamanioy = 140;
        /*String nonImage="x-wing.png";
		File fileImage=new File(nonImage);
		try{
			image=ImageIO.read(fileImage);
		}
		catch(IOException e){
			e.printStackTrace();
		}*/
        imagen = new MiImagen(1);
    }

    public int getX(){//Getter que obtiene el valor del eje x del jugador
        return this.x;
    }

    public void setX(int x){//Setter que establece el valor del eje x del jugador
        this.x = x;
    }

    public int getY()//Getter que obtiene el valor del eje y del jugador
    {
        return this.y;
    }

    public int getTamanio()//Getter que obtiene valor del tamanio del jugador
    {
        return this.tamaniox;
    }

    /**
     * Metodo paint:
     * @param g
     */
    public void paint(Graphics g)//Metodo que pinta al jugador
    {
        imagen.paint(g, this.x, this.y, this.tamaniox, this. tamanioy);
    	//g.drawImage(image, x, y, tamanio, tamanio+40, null);
       // g.setColor(Color.GREEN);
        //g.fillOval(this.x, this.y, this.tamanio, this.tamanio);
    }

	public BufferedImage getImage() {
		return image;
	}

	public void setImage(BufferedImage image) {
		this.image = image;
	}

	public ImageObserver getObserver() {
		return observer;
	}

	public void setObserver(ImageObserver observer) {
		this.observer = observer;
	}
}

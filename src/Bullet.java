import java.awt.*;
import java.awt.image.BufferedImage;
import java.awt.image.ImageObserver;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

/**
 * @author Enrique Posada y Miguel Navarro on 10/04/2017 
 * Clase Bullet: Se encarga de trazar los objetos de tipo Bullet ("Bala") en el canvas
 */

public class Bullet {
    private int x;
    private int y;
    private int tamanio;
    private BufferedImage image;
    private ImageObserver observer;
    private MiImagen imagen = new MiImagen(2);

    /**
     * Constructor Default Bullet: Este constructor se encarga de asignar la imagen que será utilizada para la bala, así como pone los valores iniciales con los que aparecerá al abrir el juego.

     */

    public Bullet()//Constructor Default
    {
        this.x = 0;
        this.y = 0;
        this.tamanio = 0;
        /*String nonImage="Red_laser.png";
        File fileImage=new File(nonImage);
        try{
            image=ImageIO.read(fileImage);
        }
        catch(IOException e){
            e.printStackTrace();
        }*/
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public int getTamanio() {
        return tamanio;
    }

    public void setTamanio(int tamanio) {
        this.tamanio = tamanio;
    }
    
    /**
     * Constructor No-Default Bullet: Este constructor se encarga de asignar la imagen que será 
 utilizada para la bala, así como establecer los valores iniciales con los que aparecerá al abrir 
 el juego con los valores que recibe como parámetros.
     * @param x
     * @param y
     */
    public Bullet(int x, int y)
    {
        this.x = x;
        this.y = y;
        this.tamanio = 25;
    }

    /**
     * Metodo paint: Método que se encarga de pintar la bala
     * @param g
     */
    public void paint(Graphics g)//Metodo que se encarga de pintar la bala
    {

    	imagen.paint(g, this.x+36, this.y, this.tamanio, this.tamanio);
        //g.setColor(Color.black);
        //g.fillOval(this.x+46, this.y+30, this.tamanio, this.tamanio);
    }
   
    
    public BufferedImage getImage() {
		return image;
	}

	public void setImage(BufferedImage image) {
		this.image = image;
	}

	public ImageObserver getObserver() {
		return observer;
	}

	public void setObserver(ImageObserver observer) {
		this.observer = observer;
	}
}

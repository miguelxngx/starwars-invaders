import javax.swing.*;
import javax.swing.Timer;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

/**
 * @author Enrique Posada y Miguel Navarro on 10/04/2017
 * Clase MiVentana: Sirve para crear la ventana en donde que contiene el canvas y todos el juego 
 (asi como tambien el menu)
 */
public class MiVentana extends JFrame {
    public static final long serialVersionUID = 1L;
    private MiCanvas miCanvas;


    /**
     * Constructor Default MiVentana: Crea la ventana en la que se estará trabajando 
     */
    public MiVentana() {//Constructor Default que genera el la ventana y agrega el canvas al mismo
        super("Star Wars Invaders");
        setLayout(new BorderLayout());
        miCanvas = new MiCanvas();
        this.add(miCanvas.getPanel(), BorderLayout.NORTH);
        this.add(miCanvas, BorderLayout.CENTER);

    }

    public MiCanvas getMiCanvas()
    {
        return this.miCanvas;
    }
}
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.awt.Graphics;

import javax.imageio.ImageIO;

/**
 * @author Enrique Posada y Miguel Navarro on 10/04/2017
 * Clase MiVentana: Clase que se encarga de desplegar las imagenes que seran usadas en el juego y 
 son asignadas a la clase que les corresponde
 */

public class MiImagen {
	private int x,y,size;
	private BufferedImage image;
	private String nonImage;
	
	/**
	 * Constructor Default MiImagen: Constructor default que se encarga que establece los valores 
	 iniciales asi como lee un archivo para el jugador.  
	 */
	MiImagen(){//Constructor Default
		x=20;
		y=50;
		size=100;
		nonImage="x-wing.png";
		File archImage=new File(nonImage);
		try{
			image=ImageIO.read(archImage);
		}
		catch(IOException e){
			e.printStackTrace();
		}
	}

	/**
	 * Constructor no-Default MiImagen: Constructor no-Default que se encarga de poder desplegar 
	 las imágenes correspondientes de cada elemento en el canvas. Por ejemplo, se despliega el 
	 fondo del juego, la imagen del jugador, de los enemigos y de las balas. Recibe como 
	 parámetro el valor asignado para la imagen que le corresponde(si es una nave entonces te 
	 dirige al archivo que contiene el archivo de la imagen). 
	 * @param val
	 */
	MiImagen(int val){//Constructor No-Default que verifica que cada imagen sea dirigida al elemento que le corresponde
		x=20;
		y=50;
		size=25;
		String nonImage = " ";
		switch(val)
		{
			case 1:
				nonImage="x-wing.png";
				break;
			case 2:
				nonImage="Red_laser.png";
				break;
			case 3:
				nonImage="starwars.png";
				break;
			case 4:
				nonImage="Light_Star_Destroyer.png";
				break;
			case 5:
				nonImage="TIE-Fighter_final.png";
				break;
			case 6:
				nonImage="SpaceBackgroundExample.jpg";
				break;
			case 7:
				nonImage="Original_image1.gif";
				break;
			case 8:
				nonImage="Original_image2.gif";
				break;
			case 9:
				nonImage="Original_image3.gif";
				break;
			case 10:
				nonImage="Original_image4.gif";
				break;
			
		}

		File archImage=new File(nonImage);
		try{
			image=ImageIO.read(archImage);
		}
		catch(IOException e){
			e.printStackTrace();
		}
	}

	/**
	 * Metodo paint: Método que se encarga de pintar las imágenes correspondientes para cada
	  elemento del juego
	 * @param g
	 * @param x
	 * @param y
	 * @param sizex
	 * @param sizey
	 */
	public void paint(Graphics g, int x, int y, int sizex, int sizey){//Metodo que se encarga de pintar las imagenes
		g.drawImage(image, x, y, sizex, sizey,null);
	}

}

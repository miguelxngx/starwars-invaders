import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.ListIterator;
import java.util.Random;
import javax.swing.*;


/**
 * @author Enrique Posada y Miguel Navarro on 10/04/2017
 * Clase MiCanvas: Sirve para deplegar todo el contenido que sera visible para el usuario en la 
 pantalla. Pinta todos los elementos del juego como el score, el jugador, los enemigos, el fondo y 
 las balas
 */

public class MiCanvas extends Canvas implements KeyListener, ActionListener {//Sirve para incluir todo el contenido desplegable en el canvas
    public static final long serialVersionUID= 1L;
    private Jugador player;
    private Bullet aux;
    private ArrayList<Bullet> balas;
    private ArrayList<Bullet> balasE;
    private ListIterator<Bullet> itrBalasE;
    private ListIterator<Bullet> itrBalas;
    private ArrayList<Enemigo> enemigos;
    private ListIterator<Enemigo> itrEnemigos;
    private Timer relojFr, relojE, relojE2, relojDisparosEnemigos;
    private int flagMover = 50;
    private int score;
    private int lifes;
    private JLabel panel;
    private Random aleatorio;
    private MiImagen imagen = new MiImagen(6);
    private boolean vivo=true;
    private MiImagen imagen2 = new MiImagen(7);
    private MiImagen imagen3 = new MiImagen(8);
    private MiImagen imagen4 = new MiImagen(9);
    private MiImagen imagen5 = new MiImagen(10);
    private HighScores scores;
    private Sound sonido;

    /**
     * Constructor Default MiCanvas: Este es el constructor default. Este se encarga de poner 
     todos los objetos en el canvas de la ventana (MiVentana).
     */
    public MiCanvas()//Constructor Default que declara los timers, colecciones, listeners y al jugador
    {
    	vivo=true;
        player = new Jugador();
        addKeyListener(this);
        balas = new ArrayList<Bullet>();
        balasE = new ArrayList<Bullet>();
        enemigos = new ArrayList<Enemigo>();
        relojFr = new Timer(40, this);
        relojFr.start();
        relojE = new Timer(10000, this);
        relojE.start();
        relojE2 = new Timer( 2500, this);
        relojE2.start();
        relojDisparosEnemigos = new Timer(3000, this);
        relojDisparosEnemigos.start();
        int flagMover = 50;
        score = 0;
        lifes = 5;
        panel = new JLabel();
        panel.setText("score: <"+ score + ">     vidas: <" + lifes+">");
        aleatorio = new Random();
        scores = new HighScores();
        sonido = new Sound("juego.wav");
        sonido.play();
    }

    //public int getHighScoreValue()
    {

    }

    public JLabel getPanel()
    {
        return this.panel;
    }

    public int getScore()
    {
        return this.score;
    }

    public int getLifes()
    {
        return this.lifes;
    }

    /**
     * Metodo paint: Este método se encarga de pintar el background del juego, asi como tambien se 
     encarga de pintar al jugador, pinta a los enemigos del juego usando un Iterator y pinta las 
     balas que vaya disparando el jugador por medio de otro listIterator.  
     * @param g
     */
    BufferedImage im=new BufferedImage(600,700, BufferedImage.TYPE_USHORT_555_RGB);
    public void paint(Graphics g)//Metodo que pinta en el canvas el fondo, el jugador, los enemigos y las balas
    {
        Graphics z=im.createGraphics();
        z.fillRect(0, 0, 600, 700);

    	if(vivo){
    	imagen.paint(z, getX(), getY(), getWidth(), getHeight());
        player.paint(z);

        Enemigo auxE;
        itrEnemigos = enemigos.listIterator();
        while(itrEnemigos.hasNext())
        {
            auxE = itrEnemigos.next();
            auxE.paint(z);
        }

        itrBalas = balas.listIterator();
        while(itrBalas.hasNext())
        {
            aux = itrBalas.next();
            aux.paint(z);
        }

        itrBalasE = balasE.listIterator();
        while(itrBalasE.hasNext())
        {
            aux = itrBalasE.next();
            aux.paint(z);
        }
    	}

    	g.drawImage(im, 0, 0, null);
    	//if(!vivo){
    		//while(true){
    		//imagen2.paint(g, getX(), getY(), getWidth(), getHeight());
    		//imagen3.paint(g, getX(), getY(), getWidth(), getHeight());
    		//imagen4.paint(g, getX(), getY(), getWidth(), getHeight());
    		//imagen5.paint(g, getX(), getY(), getWidth(), getHeight());
    		//}
    	//}
    }

    public void update(Graphics g)
    {
        paint(g);
    }

    /**
     * Metodo disparar: Este método se encarga de disparar las balas, creando un objeto de Bullet 
     y las agrega a la colección de balas
     * @param x
     * @param y
     */
    public void disparar(int x, int y)//Este método se encarga de disparar las balas, creando un objeto de Bullet y las agrega a la colección de balas
    {
        aux = new Bullet(x, y);
        balas.add(aux);
    }

    /**
     * Metodo disparosEnemigos: Metodo que se encarga de que los enemigos puedan disparar
     */
    public void disparosEnemigos()//Metodo que se encarga de realizar los disparos de los enemigos
    {
        if(enemigos.size()!=0) {
            System.out.println(enemigos.size());
            int x = aleatorio.nextInt(enemigos.size());
            int i;
            Enemigo auxE = enemigos.get(x), auxE2;
            for (i = 0; i < enemigos.size(); i++) {
                auxE2 = enemigos.get(i);
                if ((auxE.getX() == auxE2.getX()) && auxE2.getY() > auxE.getY()) {
                    auxE = auxE2;
                }
            }
            balasE.add(new Bullet(auxE.getX(), auxE.getY()));
        }
    }

    /**
     * Metodo checkColision: Este método se encarga de ver si una bala “colisiona” con uno de los 
     enemigos, en donde de ser el caso se usa un iterador para hallar el enemigo que fue golpeado 
     por la bala y lo remueve de la colección.
     * @param aux
     * @return
     */
    public boolean checkColision(Bullet aux)//Este método se encarga de ver si una bala “colisiona” con uno de los enemigos, en donde de ser el caso se usa un iterador para hallar el enemigo que fue golpeado por la bala y lo remueve de la colección
    {
        boolean flag = true;
        boolean flagCol = false;
        Enemigo auxE;
        itrEnemigos = enemigos.listIterator();
        while(itrEnemigos.hasNext() && flag) {

            auxE = itrEnemigos.next();

            if ((aux.getX() == auxE.getX()) && (aux.getY() == auxE.getY())) {
                itrEnemigos.remove();
                flag = false;
                flagCol = true;
                score += auxE.getPuntos();
                panel.setText("score: <"+ score + ">     vidas: <" + lifes+">");
            }
        }
        if(!flagCol && (aux.getY() == 0))
        {
            flagCol = true;
        }
        return flagCol;
    }

    /**
     * Metodo checkColision: Este método se encarga de ver si una bala “colisiona” con el jugador, 
     en donde de ser el caso disminuye 1 vida al jugador.
     * @param aux
     * @return
     */
    public boolean checkColisionE(Bullet aux)//Metodo que verifica si hubo colision entre la bala y el jugador 
    {
        boolean flagCol = false;
        if ((aux.getX() == player.getX()) && (aux.getY() == player.getY())) {
                lifes--;
                flagCol = true;
                panel.setText("score: <"+ score + ">     vidas: <" + lifes+">");
                
        }
        if(!flagCol && (aux.getY() >= 600))
        {
            flagCol = true;
        }
        return flagCol;
    }

    /**
     * Metodo moverAbajo: Este método se encarga de que los enemigos se desplacen hacia abajo 
     conforme llegan al límite horizontal del eje x. 
     */
    public void moverAbajo()//Mueve hacia abajo los enemigos cuando estos llegan a uno de los limites en el eje x del juego 
    {
        Enemigo auxE;
        itrEnemigos = enemigos.listIterator();
        while(itrEnemigos.hasNext())
        {
            auxE = itrEnemigos.next();
            auxE.setY(auxE.getY() + 30);
        }
    }

    /**
     * Metodo drawEnemies:Este método se encarga de agregar seis enemigos al canvas por fila.  
     */
    public void drawEnemies()//Metodo que se encarga de dibujar a los enemigos
    {
        int x = aleatorio.nextInt(100);
        int i;
        if(x<50) {
            for (i = 1; i < 6; i++) {
                enemigos.add(new Enemigo100(i * 100, 50, 10));
            }
        } else if(x<85) {
            for (i = 1; i < 6; i++) {
                enemigos.add(new Enemigo200(i * 100, 50, 10));
            }
        } else
        {
            for (i = 1; i < 6; i++) {
                enemigos.add(new Enemigo500(i * 100, 50, 10));
            }
        }

    }

    /**
     * Metodo mover: Este método se encarga de agregar seis enemigos al canvas por fila. 
     * @param value
     */
    public void mover(int value)//Se encarga de mover los enemigos de lado a lado.

    {
        Enemigo auxE;
        itrEnemigos = enemigos.listIterator();

        while( itrEnemigos.hasNext() )
        {
            auxE = itrEnemigos.next();
            auxE.setX( auxE.getX()+value );
        }
    }

    @Override
    public void keyTyped(KeyEvent e) {

    }

    /**
     * Metodo keyPressed: Metodo que se encarga de darle movimiento al jugador al asignarle 
     comandos para cuando se seleccione la tecla de arriba, la tecla de abajo, la tecla de la 
     derecha, y la tecla de la izquierda
     * @param e
     */
    @Override
    public void keyPressed(KeyEvent e) {//Metodo que habilita el movimiento del personaje con teclas para que sea comandado po el usuario
        switch(e.getKeyCode())
        {
            case 37://derecha
                if(player.getX()>0)
                {
                    player.setX( player.getX() - 50);
                }
                break;
            case 39://izquierda
                if(player.getX() + player.getTamanio() < 600)
                {
                    player.setX( player.getX() + 50);
                }
                break;
            case 38://arriba
                if(balas.size() < 5) {
                    disparar(player.getX(), player.getY());
                }
                break;
        }
    }

    @Override
    public void keyReleased(KeyEvent e) {

    }

    /**
     * metodo actionPerformed: Metodo que se encarga de todas las acciones que suceden en el 
     juego. Este metodo se encarga de desplazar a los enemigos hacia los lados y hacia abajo, asi 
     como mueve las balas y hace que desaparezcan una vez que alcanzan el limite superior de la 
     ventana
     * @param e
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        if(vivo){
    	if(e.getSource().equals(relojE))
        {
            moverAbajo();
            drawEnemies();
        }

        if(e.getSource().equals(relojE2))
        {
            mover(flagMover);
            flagMover*=-1;
        }

        if(e.getSource().equals(relojDisparosEnemigos))
        {
            disparosEnemigos();
        }

        itrBalasE = balasE.listIterator();
        itrBalas = balas.listIterator();
        if(e.getSource().equals(relojFr))
        {
            while(itrBalas.hasNext()) {
                aux = itrBalas.next();
                aux.setY(aux.getY() - 10);
                if(checkColision(aux))
                {
                    itrBalas.remove();
                }
            }
            while(itrBalasE.hasNext()) {
                aux = itrBalasE.next();
                aux.setY(aux.getY() + 10);
                if(checkColisionE(aux))
                {
                    itrBalasE.remove();
                }
            }
        }

        repaint();
    }
    
    if(lifes<=0){
    	vivo=false;
    	sonido.stop();
    }
        System.out.println(vivo);
    }
}


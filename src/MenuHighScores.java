import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.Timer;

/**
 * Created by iMigue on 03/05/2017.
 */
public class MenuHighScores extends JFrame implements ActionListener{
    private JTextField texto;
    private JLabel scoreList;
    private JPanel panel;
    private HighScores scores;
    private int score;
    private boolean flagTexto;
    private JLabel textoArriba;
    private JPanel panelArriba;
    private Timer reloj;
    private boolean cerrar = false;
    private Sound sonido;

    public MenuHighScores(int score)
    {
        super("high scores");
        this.score = score;
        setLayout(new BorderLayout());
        scores= new HighScores();
        reloj = new Timer(10000, this);
        if(scores.checkScore(this.score))
        {
            texto = new JTextField(3);
            this.add(texto, BorderLayout.SOUTH);
            texto.addActionListener(this);
        }
        else
        {
            reloj.start();
        }
        scoreList = new JLabel();
        panel = new JPanel();
        panel.add(scoreList);
        this.add(panel, BorderLayout.CENTER);
        setScores();
        textoArriba = new JLabel();
        textoArriba.setText("HIGH SCORES");
        panelArriba = new JPanel();
        panelArriba.add(textoArriba);
        this.add(panelArriba, BorderLayout.NORTH);

        flagTexto = true;
        sonido = new Sound("HighScores.wav");
        sonido.play();
    }

    public void setScores()
    {
        scoreList.setText(scores.toString());
    }



    public boolean getSignal()
    {
        return this.cerrar;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if( e.getSource() == texto && flagTexto)
        {
            scores.setScore(this.score, texto.getText());
            scores.printF();
            flagTexto = false;
            setScores();
            reloj.start();
            this.remove(texto);
        }

        if(e.getSource() == reloj)
        {
            reloj.stop();
            cerrar = true;
            sonido.stop();
        }
    }
}

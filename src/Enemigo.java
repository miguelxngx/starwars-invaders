import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

/**
 * @author Enrique Posada y Miguel Navarro on 10/04/2017
 * Clase Enemigo: Se encarga de trazar y establecer los valores de los enemigos a trazar en el canvas
 */

public class Enemigo {//Introduce todos los valor como posicion, tamanio y paint que requiere los enemigos

    protected int x, y, tamanio, puntos;

    /**
     * Constructor Default Enemigo: Constructor Default que se encarga de inicializar los valores de X, Y, y del tamaño 
     */
    public Enemigo()//Constructor Default
    {
        this.x = 100;
        this.y = 100;
        this.tamanio = 10;
    }

    /**
     * Constructor no-Default Enemigo: Constructor no-Default que se encarga de iniciar los valores de X, Y, y del tamaño
     con los valores dados y recibidos como parámetros del constructor

     * @param x
     * @param y
     * @param tamanio
     */
    public Enemigo(int x, int y, int tamanio)//Constructor No-Default
    {
        this.x = x;
        this.y = y;
        this.tamanio = tamanio+50;
    }

    public int getPuntos()//Getter que obtiene el valor de los puntos

    {
        return this.puntos;
    }

    public int getX(){//Getter que obtiene el valor del eje x para un enemigo

        return this.x;
    }

    public void setX(int x){//Setter que establece el valor del eje x para un enemigo

        this.x = x;
    }

    public int getY()//Getter que obtiene el valor del eje y para un enemigo

    {
        return this.y;
    }

    public void setY(int y)//Setter que establece el valor del eje y para un enemigo

    {
        this.y = y;
    }

    public int getTamanio()//Getter que obtiene el valor del tamaño para un enemigo

    {
        return this.tamanio;
    }

    /**
     * Metodo paint: Método que se encarga de pintar el enemigo 
     * @param g
     */
    public void paint(Graphics g)//Método que se encarga de pintar el enemigo 
    {
        g.setColor(Color.RED);
        g.fillOval(this.x+43, this.y, this.tamanio, this.tamanio);
    }
}

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import javax.swing.*;

/**
 * @author Enrique Posada y Miguel Navarro on 10/04/2017
 * Clase Menu: Sirve para incluir el menu con el cual se comenzara el juego
 */
public class Menu extends JFrame implements KeyListener{//Sirve para incluir el menu con el cual se puede comenzar el juego
    public static final long serialVersionUID= 1L;
    private MiImagen imagen;
    Timer reloj;
    private boolean signal;
    private Sound sonido;

    /**
     * Constructor Default Menu: Constructor default con el cual se despliega el menú de inicio 
     del juego
     */
    public Menu()//Constructor Default para el menu
    {
        super();
        imagen = new MiImagen(3);
        signal = false;
        addKeyListener(this);
        this.repaint();
        sonido = new Sound("menu.wav");
        sonido.play();
    }

    /**
     * Metodo paint:Método que se encarga de pintar el menu
     * @param g
     */
    public void paint(Graphics g)//Pinta la imagen del menu
    {
        imagen.paint(g,0 ,0, 600, 600);
    }

    /**
     * Metodo signal: Metodo que actúa como un getter para el atributo signal, el cual se
      encarga de que el menu este presente hasta que el valor booleano cambie de valor 
      (de falso a verdadero), dando de esa manera seguimiento para iniciar el juego
     * @return
     */
    public boolean signal()
    {
        return signal;
    }

    @Override
    public void keyTyped(KeyEvent keyEvent) {

    }

    /**
     * Metodo keyPressed: Metodo en donde se verifica que si la tecla presionada es la tecla espacio,
      entonces se cambia el valor del atributo signal para dar seguimiento con comenzar el juego
     * @paramKeyEvent e
     */
    @Override
    public void keyPressed(KeyEvent e) {//Metodo que comienza el juego si se presiona la tecla de espacio
        System.out.println(e.getKeyCode());
        if(e.getKeyCode() == KeyEvent.VK_SPACE)
        {
            signal = true;
            sonido.stop();
        }
    }

    @Override
    public void keyReleased(KeyEvent keyEvent) {

    }
}

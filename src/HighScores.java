import javax.swing.*;
import java.io.*;
import java.nio.Buffer;
import java.util.ArrayList;
import java.util.ListIterator;

/**
 * Created by iMigue on 02/05/2017.
 */
public class HighScores {
    private FileReader readFile;
    private FileWriter fw;
    private PrintWriter printFile;
    private BufferedReader reader;
    private ArrayList<Integer> scores;
    private ArrayList<String> nombres;
    private ArrayList<ArrayList> highScores;
    private ListIterator<Integer> itrScores;
    private ListIterator<String> itrNombres;
    private JLabel label;
    private JPanel panel;

    public HighScores()
    {
        try
        {
            readFile = new FileReader("HighScores.txt");
        }
        catch (Exception e)
        {
            try {
                File archivo = new File("HighScores.txt");
                archivo.createNewFile();
                fw = new FileWriter(archivo);
                printFile = new PrintWriter(fw);
                printFile.println(0);
            }
            catch(Throwable e2)
            {

            }
            finally {
                try{
                    fw.close();
                }
                catch(Exception e4)
                {

                }
                printFile.close();
            }
            try
            {
                readFile = new FileReader("HighScores.txt");
            }
            catch (Exception e3)
            {

            }

        }
        finally {
            try
            {
                readFile.close();
            }
            catch(Exception e)
            {

            }
        }
        scores  = new ArrayList<Integer>();
        nombres = new ArrayList<String>();
        highScores = new ArrayList<ArrayList>();
        giveValues();
        label = new JLabel();
        panel = new JPanel();
    }

    public void giveValues() {
        int i;
        try {
            readFile = new FileReader("HighScores.txt");
            try {
                reader = new BufferedReader(readFile);

                try {
                    String tam = reader.readLine();
                    int tamanio = Integer.parseInt(tam);

                    for (i = 0; i < tamanio; i++) {
                        scores.add(Integer.parseInt(reader.readLine()));
                    }

                    for (i = 0; i < tamanio; i++) {
                        nombres.add(reader.readLine());
                    }
                    System.out.print(3);
                    for (i = scores.size(); i < 10; i++) {
                        scores.add(0);
                        nombres.add(" ");
                    }
                } catch (Exception e) {
                    System.out.print(e.getMessage());
                }
            } catch (Exception ec) {
                for (i = 0; i < 10; i++) {
                    scores.add(0);
                    nombres.add(" ");
                }
            }
        }
        catch (Exception e)
        {

        }
        finally {
            try {
                reader.close();
                readFile.close();
            } catch (Exception e) {

            }
        }
    }

    public void print()
    {
        itrScores = scores.listIterator();
        itrNombres = nombres.listIterator();

        while(itrScores.hasNext())
        {
            System.out.println(itrScores.next());
        }

        while(itrNombres.hasNext())
        {
            System.out.println(itrNombres.next());
        }
    }

    public void setScore(int score, String nombre)
    {
        boolean flag = true;

        scores.add(score);
        nombres.add(nombre);
        int i, j;
        int aux;
        String auxS;
        for (i=0; i<scores.size() && flag; i++)
        {
            for(j=0 ; j<scores.size() ; j++) {

                if (scores.get(i) > scores.get(j)) {

                    aux = scores.get(i);
                    scores.set(i, scores. get(j));
                    scores.set(j, aux);

                    auxS = nombres.get(i);
                    nombres.set(i, nombres.get(j));
                    nombres.set(j, auxS);

                }
            }
        }
    }

    public void printF()
    {
        int i;
        try{
            File archivo = new File("HighScores.txt");
            printFile = new PrintWriter(archivo);
            printFile.println(10);

            for(i=0; i<10; i++)
            {
                printFile.println(scores.get(i));
            }
            for(i=0; i<10; i++)
            {
                printFile.println(nombres.get(i));
            }
        }catch (Exception e)
        {

        }
        finally {
            try
            {
                printFile.close();
            }
            catch(Exception e)
            {

            }
        }

    }

    public boolean checkScore(int score)
    {
        itrScores = scores.listIterator();
        boolean flag = true, flagRet = false;
        while(itrScores.hasNext() && flag)
        {
            if(score > itrScores.next())            {
                flag = false;
                flagRet = true;
            }
        }

        return flagRet;
    }

    public String toString()
    {
        return "<html><body><center>"+scores.get(0) + "        " + nombres.get(0) + "<br>" + scores.get(1) + "        " + nombres.get(1) + "<br>" +
                scores.get(2) + "        " + nombres.get(2) + "<br>" + scores.get(3) + "        " + nombres.get(3) + "<br>" +
                scores.get(4) + "        " + nombres.get(4) + "<br>" + scores.get(5) + "        " + nombres.get(5) + "<br>" +
                scores.get(6) + "        " + nombres.get(6) + "<br>" + scores.get(7) + "        " + nombres.get(7) + "<br>" +
                scores.get(8) + "        " + nombres.get(8) + "<br>" + scores.get(9) + "        " + nombres.get(9)+"</center></body></html>";
    }

    public JPanel listar()
    {
        int i;
        JLabel aux;
        for( i = 0; i<scores.size() ; i++)
        {
            aux = new JLabel();
            aux.setText(scores.get(1) + "        " + nombres.get(1));
            panel.add(aux);
        }
        return this.panel;
    }
}

import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.awt.image.ImageObserver;

/**
 * @author Enrique Posada y Miguel Navarro on 10/04/2017
 * Clase Enemigo100: Sirve para crear los enemigos que otorgan 500 puntos al ser destruidos por el jugador
 */
public class Enemigo500 extends Enemigo {
		private BufferedImage image;
	    private ImageObserver observer;
	    private MiImagen imagen = new MiImagen(4);
	    
	    /**
	     * Constructor no-Default Enemigo500: Constructor no-Default que se encarga de establecer los
	      valores de X, Y, y el tamaño con los valores que recibe como parámetros
	     * @param x
	     * @param y
	     * @param tamanio
	     */
    public Enemigo500(int x, int y, int tamanio)
    {
        super(x, y, tamanio);
        this.puntos = 500;
    }
    /**
     * Metodo paint: Método que se encarga de pintar el enemigo 
     * @param g
     */
    public void paint(Graphics g)//Método que se encarga de pintar el enemigo 

    {

    	imagen.paint(g, this.x+36, this.y, this.tamanio+5, this.tamanio+10);
        //g.setColor(Color.black);
        //g.fillOval(this.x+46, this.y+30, this.tamanio, this.tamanio);
    }
   
    
    public BufferedImage getImage() {
		return image;
	}

	public void setImage(BufferedImage image) {
		this.image = image;
	}

	public ImageObserver getObserver() {
		return observer;
	}

	public void setObserver(ImageObserver observer) {
		this.observer = observer;
	}

}
